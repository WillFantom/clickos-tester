import os, signal, time, uuid
from subprocess import call, Popen, PIPE

class Commander():

  def __init__(self):
    self.jobs = {}

  def __update_jobs(self):
    for job in self.jobs:
      job["returncode"] = job.get("popen").poll()

  def run(self, command_string, pipe_output=True, wait=True):
    command_array = command_string.split(' ')
    job_id = str(uuid.uuid4())
    job = {}
    job["returncode"] = None
    job["piped"] = (True if pipe_output else False)
    job["popen"] = None
    if pipe_output:
      job["popen"] = Popen(command_array, stdout=PIPE, stderr=PIPE)
    else:
      job["popen"] = Popen(command_array)
    self.jobs[job_id] = job
    if wait:
      self.wait_for_job(job_id)
    return job_id

  def wait_for_job(self, jid, kill_after=None):
    if jid in self.jobs:
      count = 0
      while self.jobs.get(jid).get("popen").poll() == None:
        time.sleep(0.1)
        count += 1
        if kill_after:
          if count == (kill_after*10):
            self.jobs.get(jid).get("popen").kill()
    return

  def get_return(self, jid):
     if jid in self.jobs:
       self.jobs.get(jid).update({"returncode": self.jobs.get(jid).get("popen").poll() })
       return self.jobs.get(jid).get("returncode")

  def get_output(self, jid, err=False):
    if jid in self.jobs:
      self.jobs.get(jid).update({"returncode": self.jobs.get(jid).get("popen").poll() })
      if self.jobs.get(jid).get("piped") == True:
        stdout,stderr = self.jobs.get(jid).get("popen").communicate()
        if err:
          return stderr
        return stdout
    return ""
