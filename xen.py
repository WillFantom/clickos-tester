import _xenstat as xs
from commander import Commander
import os
import logging
import json
import time

class XenStat():

  def __init__(self):
    self.handle = xs.xenstat_init()

  def monitor_domain(self, did, freq, duration):
    cpu_pct = []
    node = xs.xenstat_get_node(self.handle, xs.XENSTAT_ALL)
    domain = xs.xenstat_node_domain(node, did)
    c_o = xs.xenstat_domain_cpu_ns(domain)
    t_o = time.time() * 1000000
    for i in range(0, int(duration/freq)):
      time.sleep(freq)
      xs.xenstat_free_node(node)
      node = xs.xenstat_get_node(self.handle, xs.XENSTAT_ALL)
      domain = xs.xenstat_node_domain(node, did)
      elapsed_cpu = xs.xenstat_domain_cpu_ns(domain) - c_o
      elapsed_time = (time.time() * 1000000) - t_o
      pct = (elapsed_cpu/10.0)/(elapsed_time)
      cpu_pct.append(pct if pct<=100 else 100)
      c_o = c_o + elapsed_cpu
      t_o = t_o + elapsed_time
    xs.xenstat_free_node(node)
    return {
      "cpu": cpu_pct,
      "avg": sum(cpu_pct)/len(cpu_pct)
    }

class XenLight():

  XL_PATH = "/usr/local/sbin/xl"

  KERNEL_BASE_DEFAULT = "./images/"

  CNF_TEMPLATE_PATH = "./templates/xen-basic.cnf"
  NIC_STRING = "\"mac={},script=vif-vale,bridge={}\""

  XL_LIST = "{} list -l"
  XL_DOMNAME = "{} domname {}"
  XL_DOMID = "{} domid {}"
  XL_CREATE = "{} create -f {} {} {} {}"
  XL_DESTROY = "{} destroy {}"
  XL_VCPUPIN = "{} vcpu-pin {} 0 {} {}"
  XL_CONSOLE = "{} console {}"

  def __init__(self):
    self.runner = Commander()

  def __modify_template(self, kernel_path, domain_name, nics):
    if nics >= 10 or nics < 0: return True
    if not os.path.isfile(kernel_path): return True
    with open(self.CNF_TEMPLATE_PATH, "r") as tmp:
      template = tmp.read()
    template = template.replace("$$KERNEL_PATH$$", kernel_path, 1)
    template = template.replace("$$DOM_NAME$$", domain_name, 1)
    nic_str="\""
    for n in range(0, nics):
      nic_str = nic_str + self.NIC_STRING.format("00:00:00:00:aa:0"+str(n), "vale"+str(n)) + ","
    nic_str = nic_str + "\""
    template = template.replace("$$NIC_STRINGS$$", nic_str, 1)
    if os.path.exists("./tmp/xen.cnf"):
      os.remove("./tmp/xen.cnf")
    with open("./tmp/xen.cnf", 'w+') as cnf:
      cnf.write(template)
      return True

  def __config_kernel(self, kernel_path):
    kernel_path = self.KERNEL_BASE_DEFAULT + kernel_path
    if not os.path.isfile(kernel_path):
      return None,"no kernel exists at this path"
    return "kernel=\"{}\"".format(kernel_path),None

  def __config_domain_name(self, name):
    return "name=\"{}\"".format(name)

  def __config_nics(self, count):
    nic_string = ""
    for n in range(0, count):
      nic_string += self.NIC_STRING.format("00:00:00:00:aa:0"+str(n), "vale"+str(n))
      if n!=(count-1): nic_string += "," 
    return "vif=[{}]".format(nic_string),None

  def create_domain(self, kernel, domain_name, nics=2):
    ''' returns dom_id,err '''
    kpath,err = self.__config_kernel(kernel)
    if not err == None: return None,err
    dom_name = self.__config_domain_name(domain_name)
    nic_str,err = self.__config_nics(nics)
    if not err == None: return None,err
    command_string = self.XL_CREATE.format(self.XL_PATH, self.CNF_TEMPLATE_PATH, kpath, dom_name, nic_str)
    jid = self.runner.run(command_string, pipe_output=False, wait=True)
    if not self.runner.get_return(jid) == 0:
      return None,"xl create failed"
    command_string = self.XL_DOMID.format(self.XL_PATH, domain_name)
    jid = self.runner.run(command_string, pipe_output=True, wait=True)
    output = self.runner.get_output(jid)
    if output == "": return None,"xl domname to id failed"
    time.sleep(1)
    print(output)
    return int(output),None

  def destroy_domain(self, domain):
    ''' returns err '''
    command_string = self.XL_DESTROY.format(self.XL_PATH, domain)
    jid = self.runner.run(command_string, pipe_output=False, wait=True)
    if not self.runner.get_return(jid) == 0:
      return "xl destroy failed"
    return None

  def pin_cpu(self, domain_id, pcpu):
    ''' returns err '''
    command_string = self.XL_VCPUPIN.format(self.XL_PATH, domain_id, pcpu, pcpu)
    jid = self.runner.run(command_string, pipe_output=False, wait=True)
    if not self.runner.get_return(jid) == 0:
      return "xl vcpu pin failed"
    return None

  def get_domain_info(self, domid):
    command_string = self.XL_LIST.format(self.XL_PATH)
    jid = self.runner.run(command_string, pipe_output=True, wait=True)
    if not self.runner.get_return(jid) == 0:
      return None,"xl list failed"
    output = self.runner.get_output(jid)
    if output == "":
      return None,"xl list returned nothing"
    domain_info = json.loads(output)
    for domain in domain_info:
      if int(domain.get("domid")) == domid:
        return domain,None
    return None,"domain info could not be found"

  def get_console_out(self, domid):
    ''' returns consoleoutput,err '''
    command_string = self.XL_CONSOLE.format(self.XL_PATH, domid)
    jid = self.runner.run(command_string, pipe_output=True, wait=False)
    self.runner.wait_for_job(jid, kill_after=1)
    output = self.runner.get_output(jid)
    if output == "":
      return None,"xl console returned nothing"
    return output,None
